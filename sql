from flask import Flask
from flask import session
from flask import render_template
from flask import request
from flask import url_for
from flask import flash, redirect, g
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from random import randint
import sqlite3
import os
from passlib.hash import sha256_crypt

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
code1=0
code2=0

couple={}

val = randint(1000000,999999999999)

app=Flask(__name__)

app.secret_key="some secret"
'''
@app.before_request
def before_request():
	g.username=None
	if 'username' in session:
		g.username = session['username']
'''
@app.route('/')
def index():
	if 'username' in session:
		username = session['username']
		return redirect('/home/')
	return render_template("index.html")


@app.route('/signup')
def signup():
	return render_template('signup.html')

@app.route("/upload" , methods = ['POST'])
def upload():
	target = os.path.join(APP_ROOT , "images/")
	print (target)
	username = session['username']

	if not os.path.isdir(target):
		os.mkdir(target)

	for file in request.files.getlist("file"):
		print file
		filename = str(file.filename)
		filee = filename.split(".")
		filee[0]=username
		filename = ".".join(filee)
		destination = "/".join([target , filename])
		print(destination)
		file.save(destination)

	return redirect("/home/")




@app.route('/everify' ,methods=['POST'])
def everify():
	
	couple["phone1"] = request.form['phone1']
	couple["phone2"] = request.form['phone2']
	couple["us1"] = request.form['uname1']
	couple["us2"] = request.form['uname2']
	print(couple["us1"])
	couple["email1"] = request.form['email1']
	couple["email2"] = request.form['email2']
	email1 = request.form['email1']
	email2 = request.form['email2']
	conn = sqlite3.connect('data.db')
	cursor = conn.cursor()
	row=None
	cursor.execute("SELECT * FROM users WHERE USERNAME = ? OR USERNAME = ?",(couple['us1'],couple['us2']))
	row = cursor.fetchone()
	if row:
		flash("Username already exists")
		return redirect('/signup')
	cursor.execute("SELECT * FROM users WHERE EMAIL = ? OR EMAIL = ?",(couple['email1'],couple['email2']))
	row= cursor.fetchone()
	if row:
		flash("Email already registered")
		return redirect('/signup')

	couple["psw1"] = request.form['psw1']
	couple["psw2"] = request.form['psw2']
	couple["cpsw1"] = request.form['cpsw1']
	couple["cpsw2"] = request.form['cpsw2']

	if couple["psw1"]!=couple["cpsw1"] or couple["psw2"]!=couple["cpsw2"]:
		flash('Passwords do not match')
		return redirect('/signup')
	couple["age1"]=request.form["age1"]
	couple["age2"]=request.form["age2"]
	
	couple["gender1"]=request.form["gender1"]
	couple["gender2"]=request.form["gender2"]

	couple["fname1"] = request.form['fname1']
	#print(couple["fname1"])
	couple["fname2"] = request.form['fname2']
	couple["lname1"] = request.form['lname1']
	couple["lname2"] = request.form['lname2']
	if couple["email1"] == couple["email2"]:
		flash('Same email entered')
		return redirect('/signup')

	global code1
	code1 = randint(1000000,9999999)
	TEXT1 = "Your verification code is " + str(code1)
	SUBJECT = "Verification Code"
	msg1 = 'Subject: {}\n\n{}'.format(SUBJECT, TEXT1)

	global code2
	code2 = randint(1000000,9999999)
	TEXT2 = "Your verification code is " + str(code2)
	msg2 = 'Subject: {}\n\n{}'.format(SUBJECT, TEXT2)


	#Create login Server
	server = smtplib.SMTP('smtp.googlemail.com', 587)
	server.starttls()
	server.ehlo()
	server.login("coupledal@gmail.com", "ShubhAksh@123")
	 
	#Send Mail
	user1 = email1
	user2 = email2
	server.sendmail("coupledal@gmail.com",user1, msg1)
	server.sendmail("coupledal@gmail.com",user2, msg2)
	server.quit()
	global val
	val = randint(10000000,99999999999999)
	return redirect(url_for('enter_code',value = val))

@app.route('/enter_code/<value>')
def enter_code(value):
	#print(couple["us1"])
	#print(couple["fname1"])
	return render_template('everify.html',u1=couple["fname1"],u2=couple["fname2"],val =value)


@app.route('/home/')
def home():
	
	if 'username' in session:
		un = session['username']
		conn = sqlite3.connect('data.db')
		cursor = conn.cursor()
		row=None
		cursor.execute("SELECT * FROM couples WHERE FIRST = ?",[un])
		row = cursor.fetchone()
		un1=row[1]
		cursor.execute("SELECT * FROM users WHERE USERNAME = ?",[un1])
		row = cursor.fetchone()
		fname=row[1]
		cursor.execute("SELECT * FROM users WHERE USERNAME = ?",[un])
		row = cursor.fetchone()
		desc = row[10]
		flag=row[9]
		flag = int(flag)
		if flag==0:
			cursor.execute("UPDATE users SET FLAG = 1 WHERE USERNAME = ?", [un])
			conn.commit()
			return render_template('upload.html')
		return render_template('home.html',partner=fname,bio=desc)
   	return redirect('/')

@app.route('/forgot_password')
def forgot():
	global code1
	code1 = randint(1000000,9999999)
	TEXT1 = "Your verification code is " + str(code1)
	SUBJECT = "Verification Code"
	msg1 = 'Subject: {}\n\n{}'.format(SUBJECT, TEXT1)


	#Create login Server
	server = smtplib.SMTP('smtp.googlemail.com', 587)
	server.starttls()
	server.ehlo()
	server.login("coupledal@gmail.com", "ShubhAksh@123")
	 
	#Send Mail
	user1 = email1
	server.sendmail("coupledal@gmail.com",user1, msg1)
	server.quit()
	
	


@app.route('/login/', methods=['GET','POST'])
def login():
	if request.method=='GET':
		return redirect('/')
	conn = sqlite3.connect('data.db')
	cursor = conn.cursor()
	row=None
	un=request.form["uname"]
	cursor.execute("SELECT * FROM users WHERE USERNAME = ?",[un])
	row = cursor.fetchone()
	if row==None:
		flash("Username does not exist")
		return redirect("/")
	if sha256_crypt.verify(request.form['psw'],row[5]):
		session['username'] = request.form['uname']
		return redirect("/home/")
	else:
		flash("Incorrect password")
		return redirect("/")


@app.route('/verify/<val>', methods=['POST'])
def verify(val):
	try:
		code1e=int(request.form['ecode1'])
		code2e=int(request.form['ecode2'])

		if code1==code1e and code2==code2e:
			conn = sqlite3.connect('data.db')
			couple["psw1"] = sha256_crypt.encrypt(couple["psw1"])
			couple["psw2"] = sha256_crypt.encrypt(couple["psw2"])

			print(couple['psw1'])
			print(sha256_crypt.encrypt(couple['psw1']))
			try:
				cur = conn.cursor()
				cur.execute("INSERT INTO users (USERNAME,FIRST NAME,LAST NAME,MOBILE,EMAIL,PASSWORD,AGE,GENDER,FLAG) VALUES (?,?,?,?,?,?,?,?,?)", (couple["us1"],couple["fname1"],couple["lname1"],couple["phone1"],couple["email1"],couple["psw1"],couple["age1"],couple["gender1"],0))
				cur.execute("INSERT INTO users (USERNAME,FIRST NAME,LAST NAME,MOBILE,EMAIL,PASSWORD,AGE,GENDER,FLAG) VALUES (?,?,?,?,?,?,?,?,?)", (couple["us2"],couple["fname2"],couple["lname2"],couple["phone2"],couple["email2"],couple["psw2"],couple["age2"],couple["gender2"],0))
				cur.execute("INSERT INTO couples (FIRST,SECOND) VALUES(?,?)" , (couple['us1'],couple['us2']))
				cur.execute("INSERT INTO couples (FIRST,SECOND) VALUES(?,?)" , (couple['us2'],couple['us1']))					
			except:
			#print("Error aa gaya bc")
				conn.rollback()
			
			conn.commit()
			flash("You have registered successfully.")
			return redirect('/')

		else:
			flash('Verification Failed')
			return redirect(url_for('enter_code',value = val))

	except Exception as e:	
		flash('Verification Failed')
		return redirect(url_for('enter_code',value = val))

@app.route('/logout/')
def logout():
	session.pop('username',None)
	return redirect('/')
@app.route('/edit_bio/',methods=['GET','POST'])
def edit_bio():
	if request.method=='GET':
		return redirect('/home/')
	bio = request.form['bio']
	username = session['username']
	conn = sqlite3.connect('data.db')
	cursor = conn.cursor()
	cursor.execute("UPDATE users SET BIO = ? WHERE USERNAME = ?", (bio,username))
	conn.commit()
	return redirect('/home/')
if __name__=='__main__':
    app.run(debug=True)

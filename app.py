from flask import Flask,render_template,flash,redirect,url_for,session,logging,request
from flask_mail import Mail,Message
from flask_mysqldb import MySQL
from wtforms import Form,StringField,TextAreaField,PasswordField,validators
from passlib.hash import sha256_crypt
from functools import wraps
from itsdangerous import URLSafeTimedSerializer,SignatureExpired
import os

app=Flask(__name__)
name1=""
usernname1=""
email1=""
password1=""
username2=""
app.config['MYSQL_HOST']='localhost'
app.config['MYSQL_USER']='root'
app.config['MYSQL_PASSWORD']='9858267595'
app.config['MYSQL_DB']='mymusic'
app.config['MYSQL_CURSORCLASS']='DictCursor'

app.config.from_pyfile('config.cfg')
mail=Mail(app)

s=URLSafeTimedSerializer('secret123')

mysql=MySQL(app)


@app.route('/')
def index():
	return render_template('index.html')
@app.route('/Artists')
def artists():
	return render_template("artists.html")
@app.route('/Albums')
def albums():
	return render_template("album.html")
@app.route('/Dashboard')
def playlist():
	cur=mysql.connection.cursor()

	result=cur.execute("SELECT * from songs")

	songs=cur.fetchall()
	if result>0:
		return render_template('Dashboard.html',songs=songs)
	else:
		msg="NO PLAYLIST FOUND "

	return render_template('Dashboard.html',msg=msg)
	cut.close()


class RegisterForm(Form):
	name=StringField('Name',[validators.Length(min=1,max=50)])
	username=StringField('Username',[validators.Length(min=4,max=25)])
	email=StringField('Email',[validators.Length(min=6,max=50)])
	password=PasswordField('Password',[validators.DataRequired(),validators.EqualTo('confirm',message='Password do not match')])
	confirm=PasswordField('confirm Password')

@app.route('/register',methods=['GET','POST'])
def register():
	form =RegisterForm(request.form)
	if request.method=='POST' and form.validate():
		name=form.name.data
		email=form.email.data
		username=form.username.data
		password=sha256_crypt.encrypt(str(form.password.data))
		global usernname1,name1,email1,password1
		usernname1=username
		name1=name
		email1=email
		password1=password

		token=s.dumps(email,salt='email-confirm')

		msg=Message('Confirm Email',sender='akhil1.as42@gmail.com',recipients=[email])

		link=url_for('confirm_email',token=token,_external=True)

		msg.body='Your link is {}'.format(link)

		mail.send(msg)

		cur=mysql.connection.cursor()
		result=cur.execute("SELECT * FROM users WHERE username= %s",[username])
		result2=cur.execute("SELECT * FROM users WHERE email=%s",[email])
		if result>0:
			error='User name already exists,please try another user name'
			return render_template('register.html',form=form,error=error)
		#if result2>0:
		#	error='Email already exists,please try another Email'
		#	return render_template('register.html',form=form,error=error)
		else:
			flash('A confirmation link has been sent to your email','success')
		return redirect(url_for('index'))

	return render_template('register.html',form=form)

@app.route('/confirm_email/<token>')
def confirm_email(token):
	cur=mysql.connection.cursor()
	try:
		email=s.loads(token,salt='email-confirm',max_age=3600)
	except SignatureExpired:
		flash('The confirmation link is invalid or has expired.','danger')
	else:
		cur.execute("INSERT INTO users(name,email,username,password) VALUES(%s,%s,%s,%s)",(name1,email1,usernname1,password1))
		mysql.connection.commit()
		cur.close()
		flash('Successfully verified','success')
	return redirect(url_for('login'))




@app.route('/login',methods=['GET','POST'])
def login():
	if request.method=='POST':
		username=request.form['username']

		password_candidate=request.form['password']

		cur=mysql.connection.cursor()

		result=cur.execute("SELECT * FROM users WHERE username= %s",[username])

		if result>0:
			data=cur.fetchone()
			password=data['password']

			if sha256_crypt.verify(password_candidate,password):
				session['logged_in']=True
				session['username']=username
				session['id']=data['id']

				flash('login successful','success')
				return redirect(url_for('dashboard'))
			else:
				error='wrong password'
			return render_template('login.html',error=error)
			cur.close()
		else:
			error='Username not found'
			return render_template('login.html',error=error)

	return render_template('login.html')
def is_logged_in(f):
	@wraps(f)
	def wrap(*args,**kwargs):
		if 'logged_in' in session:
			return f(*args,**kwargs)
		else:
			flash('unauthorised,please login','danger')
			return redirect(url_for('login'))
	return wrap
@app.route('/logout')
def logout():
	session.clear()
	flash('you are now logout','success')
	return redirect(url_for('login'))

@app.route('/dashboard')
@is_logged_in
def dashboard():
	cur=mysql.connection.cursor()

	result=cur.execute("SELECT * from songs WHERE user_id = %s",[session['id']])

	songs=cur.fetchall()

	if result>0:
		return render_template('dashboard.html',songs=songs)
	else:
		msg="NO PLAYLIST FOUND "

	return render_template('dashboard.html',msg=msg)
	cur.close()

class make_playlist(Form):
	title=StringField('Name',[validators.Length(min=1,max=25)])


@app.route('/create_playlist',methods=['GET','POST'])
@is_logged_in
def create_playlist():
	form=make_playlist(request.form)
	if request.method=='POST' and form.validate():
		title=form.title.data

		cur=mysql.connection.cursor()

		#result=cur.execute("SELECT * FROM users WHERE username= %s",[{{session.username}}])
		username=session['username']

		row=cur.execute("SELECT * FROM users WHERE username = %s",[username])
		result=cur.fetchone()
		idd=result['id']
		cur.execute("INSERT INTO songs(title,user_id) VALUES (%s,%s)",([title],idd))
		mysql.connection.commit()
		cur.close()

		flash(idd,'success')

		return redirect(url_for('dashboard'))
	return render_template('add_playlist.html',form=form)

@app.route('/Reputation')
@is_logged_in
def play():
    to_search="Reputation"
    cur=mysql.connection.cursor()
    cur.execute("SELECT * FROM songs_list WHERE album LIKE 'Rep%'")
    albu=cur.fetchall()
    return render_template('home.html',albu=albu)
@app.route('/Camila')
@is_logged_in
def cam():
	to_search="Camila Cabello"
	cur=mysql.connection.cursor()
	cur.execute("SELECT * FROM songs_list WHERE album LIKE '%Cami%'")
	albu1=cur.fetchall()
#	app.logger.info(albu[0]["path"])
	return render_template('camila.html',albu=albu1)
@app.route('/CTRL')
@is_logged_in
def sza():
    to_search="SZA - CTRL"
    cur=mysql.connection.cursor()
    cur.execute("SELECT * FROM songs_list WHERE album LIKE 'SZA%'")
    albu2=cur.fetchall()
    return render_template('ctrl.html',albu=albu2)
@app.route('/BlackPanther')
@is_logged_in
def panther():
    to_search="black-panther"
    cur=mysql.connection.cursor()
    cur.execute("SELECT * FROM songs_list WHERE album LIKE 'bla%'")
    albu3=cur.fetchall()
	#print(albu3[11]["path"]
    return render_template('blackpanther.html',albu=albu3)
@app.route('/Damn')
@is_logged_in
def damn():
    to_search="Kendrick Lamar - DAMN."
    cur=mysql.connection.cursor()
    cur.execute("SELECT * FROM songs_list WHERE album LIKE 'Ken%'")
    albu4=cur.fetchall()
#    app.logger.info(albu[11]["path"]
    return render_template('damn.html',albu=albu4)
@app.route('/SGFG')
@is_logged_in
def sgfg():
    to_search="5 Seconds Of Summer"
    cur=mysql.connection.cursor()
    cur.execute("SELECT * FROM songs_list WHERE album LIKE '5 S%'")
    albu5=cur.fetchall()
#	app.logger.info(albu5[14]["path"])
    return render_template('sgfg.html',albu=albu5)

@app.route('/revival')
@is_logged_in
def revival():
	cur=mysql.connection.cursor()
	cur.execute("SELECT * FROM songs_list WHERE album LIKE 'Emi%'")
	albu6=cur.fetchall()
	result=cur.execute("SELECT * from songs WHERE user_id = %s",[session['id']])
	songs=cur.fetchall()
	if result>0:
		return render_template('revival.html',songs=songs,albu=albu6)
	else:
		songs=0
		return render_template('revival.html',albu=albu6,song=songs)
	cur.close()
#	app.logger.info(albu5[14]["path"])
@app.route('/ManOfWoods')
@is_logged_in
def manof():
    to_search="Man Of The Woods"
    cur=mysql.connection.cursor()
    cur.execute("SELECT * FROM songs_list WHERE album LIKE 'Jus%'")
    albu7=cur.fetchall()
#	app.logger.info(albu5[14]["path"])
    return render_template('manofwoods.html',albu=albu7)
@app.route('/save_playlist/<string:name>/<string:ide>')
@is_logged_in
def save(name,ide):
	cur=mysql.connection.cursor()
	cur.execute("SELECT * FROM songs WHERE title = %s",[name])
	cur.fetchone()
#	cur.execute("UPDATE songs SET _songs=CONCAT('',_songs) WHERE title=%s",(name));
	cur.execute("UPDATE songs SET _songs=CONCAT(%s'',_songs) WHERE title=%s",(ide,name))
#	cur.execute("UPDATE songs SET _songs =%s WHERE title=%s",(ide,name))
	mysql.connection.commit()
	cur.close()
	return redirect(url_for('revival'))
@app.route('/play_playlist/<string:idd>')
@is_logged_in
def play_playlist(idd):
	cur=mysql.connect.cursor()
	cur.execute("SELECT * FROM songs WHERE id =%s",[idd])
	result=cur.fetchone()
	result['_songs']
	app.logger.info(result['_songs'])
	return render_template('dashboard.html')



if __name__=='__main__':
	app.secret_key='secret123'
	app.run(debug=True)